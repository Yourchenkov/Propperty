package StNetYourchenkov;

import StNetYourchenkov.Propperty;

public class Table extends Propperty {
    private String material;
    private int size;

    public Table(String coler, String vendor, int price) {
        super(coler, vendor, price);
    }

    public Table(String coler, String vendor, int price, String material, int size) {
        super(coler, vendor, price);
        this.material = material;
        this.size = size;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StNetYourchenkov.Table{");
        sb.append("material='").append(material).append('\'');
        sb.append(", size=").append(size);
        sb.append(", coler='").append(coler).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
    @Override
    public void getInfo() {
        System.out.println(toString());
    }
}
