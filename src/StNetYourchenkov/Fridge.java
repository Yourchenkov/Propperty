package StNetYourchenkov;

public class Fridge extends Propperty {
    private int cameraCount;
    private String volume;
    private String energyConsClass;

    public Fridge(String coler, String vendor, int price) {
        super(coler, vendor, price);
    }

    public Fridge(String coler, String vendor, int price, int cameraCount, String volume, String energyConsClass) {
        super(coler, vendor, price);
        this.cameraCount = cameraCount;
        this.volume = volume;
        this.energyConsClass = energyConsClass;
    }

    public int getCameraCount() {
        return cameraCount;
    }

    public void setCameraCount(int cameraCount) {
        this.cameraCount = cameraCount;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getEnergyConsClass() {
        return energyConsClass;
    }

    public void setEnergyConsClass(String energyConsClass) {
        this.energyConsClass = energyConsClass;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StNetYourchenkov.Fridge{");
        sb.append("cameraCount=").append(cameraCount);
        sb.append(", volume='").append(volume).append('\'');
        sb.append(", energyConsClass='").append(energyConsClass).append('\'');
        sb.append(", coler='").append(coler).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
    @Override
    public void getInfo() {
        System.out.println(toString());
    }
}
