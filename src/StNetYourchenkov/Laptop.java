package StNetYourchenkov;

public class Laptop extends Propperty {
    private String character;

    private Laptop(String coler, String vendor, int price) {
        super(coler, vendor, price);
    }

    public Laptop(String coler, String vendor, int price, String character) {
        super(coler, vendor, price);
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StNetYourchenkov.Laptop{");
        sb.append("character='").append(character).append('\'');
        sb.append(", coler='").append(coler).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void getInfo() {
        System.out.println(toString());
    }
}
